#ifndef TEST_H
#define TEST_H

#include <QObject>

class SheetModel;
class VersionModel;
class FinalScoreModel;
class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = nullptr);

    QString testName() const;

    VersionModel *versionModel() const;
    FinalScoreModel *scoreModel() const;
    SheetModel *sheetModel() const;

    bool isChanged() const;

    QString filePath() const;

signals:

public slots:
    void clearScore();
    void setTestName(const QString &testName);
    bool load(const QString &filePath);
    void save();
    void save(const QString &filePath);
    void onCompleteDetect(const QString &uniId,
                          const QString &title,
                          const QString &filePath,
                          const QList<QString> &answer);

private slots:
    void onChange();

private:
    QString m_testName, m_filePath;
    VersionModel *m_versionModel;
    FinalScoreModel *m_scoreModel;
    SheetModel *m_sheetModel;
    bool m_isChanged;
};

#endif // TEST_H
