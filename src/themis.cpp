#include <QImage>

#include "sheetmodel.h"
#include "paperdetection.h"

#include "themis.h"

#include <QDebug>

#define checkStop() \
    if(m_stop) \
    { \
        emit finish(); \
        reset(); \
        return; \
    }

Themis::Themis(QObject *parent) : QObject(parent),
    m_stop(false),
    m_working(false)
{
}

bool Themis::isWorking() const
{
    return m_working;
}

void Themis::stop()
{
    if(m_working)
    {
        m_stop = true;
    }
}

void Themis::startTesting()
{
    if(m_candidates.isEmpty())
    {
        //Finish.
        emit finish();
        return;
    }
    // Set working flag.
    m_working = true;
    // Load and check things.
    while(!m_candidates.isEmpty())
    {
        // Load the paper.
        checkStop();
        QString filePath = m_candidates.takeFirst();
        QImage candidatePaper(filePath);
        // Binarize.
        checkStop();
        candidatePaper = PaperDetection::binarize(candidatePaper);
        // Correct the horizontal of the paper.
        checkStop();
        PaperDetection::correctHorizontal(candidatePaper);
        // Read the information of the paper.
        checkStop();
        QString uniId = PaperDetection::readUniId(candidatePaper);
        // Read the answer of the paper.
        checkStop();
        QList<QString> answer = PaperDetection::readAnswer(candidatePaper);
        // Read the information of the paper.
        checkStop();
        QString title = PaperDetection::readTitle(candidatePaper);
        // Emit the complete detection.
        emit completeDetect(uniId, title, filePath, answer);
    }
    // Finish mission.
    emit finish();
    reset();
}

void Themis::reset()
{
    m_stop = false;
    m_working = false;
}

void Themis::setSheetModel(SheetModel *sheetModel)
{
    m_candidates = sheetModel->sheets();
}
