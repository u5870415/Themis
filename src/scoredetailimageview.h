#ifndef SCOREDETAILIMAGEVIEW_H
#define SCOREDETAILIMAGEVIEW_H

#include <QDockWidget>

class ScoreDetailImageView : public QDockWidget
{
    Q_OBJECT
public:
    explicit ScoreDetailImageView(QWidget *parent = nullptr);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    QPixmap m_image;
};

#endif // SCOREDETAILIMAGEVIEW_H
