#include <QFormLayout>
#include <QLabel>

#include "clickablelabel.h"

#include "scoredetailview.h"

ScoreDetailView::ScoreDetailView(QWidget *parent) :
    QDockWidget(parent),
    m_filePath(new ClickableLabel(this)),
    m_uniId(createLabel()),
    m_version(createLabel()),
    m_answer0(createLabel(Qt::AlignRight | Qt::AlignVCenter)),
    m_answer1(createLabel(Qt::AlignRight | Qt::AlignVCenter)),
    m_answer2(createLabel(Qt::AlignRight | Qt::AlignVCenter)),
    m_answer3(createLabel(Qt::AlignRight | Qt::AlignVCenter)),
    m_answerC0(createLabel()),
    m_answerC1(createLabel()),
    m_answerC2(createLabel()),
    m_answerC3(createLabel()),
    m_score(createLabel())
{
    setWindowTitle("Paper Detail");
    // Set the container.
    QWidget *container=new QWidget(this);
    setWidget(container);
    // Configure the main layout.
    QFormLayout *mainLayout = new QFormLayout(container);
    m_filePath->setTextInteractionFlags(Qt::TextSelectableByMouse |
                                        Qt::TextSelectableByKeyboard);
    mainLayout->addRow("Uni ID", m_uniId);
    mainLayout->addRow("Path", m_filePath);
    mainLayout->addRow("Version", m_version);
    mainLayout->addRow("Answer 0", createAnswerLayout(m_answer0, m_answerC0));
    mainLayout->addRow("Answer 1", createAnswerLayout(m_answer1, m_answerC1));
    mainLayout->addRow("Answer 2", createAnswerLayout(m_answer2, m_answerC2));
    mainLayout->addRow("Answer 3", createAnswerLayout(m_answer3, m_answerC3));
    mainLayout->addRow("Score", m_score);
}

void ScoreDetailView::clearDetail()
{
    m_uniId->clear();
    m_version->clear();
    m_answer0->clear();m_answerC0->clear();
    m_answer1->clear();m_answerC1->clear();
    m_answer2->clear();m_answerC2->clear();
    m_answer3->clear();m_answerC3->clear();
    m_score->clear();
}

void ScoreDetailView::setDetail(const QString &uniId, const QString &version,
                                const QString &filePath,
                                const QString &ans0, const QString &ansc0,
                                const QString &ans1, const QString &ansc1,
                                const QString &ans2, const QString &ansc2,
                                const QString &ans3, const QString &ansc3,
                                const QString &score)
{
    m_uniId->setText(uniId);
    m_version->setText(version);
    m_filePath->setText(filePath);
    m_answer0->setText(ans0);m_answerC0->setText(ansc0);
    m_answer1->setText(ans1);m_answerC1->setText(ansc1);
    m_answer2->setText(ans2);m_answerC2->setText(ansc2);
    m_answer3->setText(ans3);m_answerC3->setText(ansc3);
    m_score->setText(score);
}

inline QLabel *ScoreDetailView::createLabel(Qt::Alignment align)
{
    QLabel *label=new QLabel(this);
    label->setAlignment(align);
    label->setTextInteractionFlags(Qt::TextSelectableByMouse |
                                   Qt::TextSelectableByKeyboard);
    return label;
}

inline QLayout *ScoreDetailView::createAnswerLayout(QLabel *ans, QLabel *cans)
{
    QBoxLayout *layout=new QBoxLayout(QBoxLayout::LeftToRight);
    layout->addWidget(ans, 1);
    layout->addWidget(new QLabel("/", this));
    layout->addWidget(cans, 1);
    return layout;
}
