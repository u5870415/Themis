#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "finalscoremodel.h"
#include "sheetmodel.h"
#include "versionmodel.h"

#include "test.h"

#include <QDebug>

Test::Test(QObject *parent) : QObject(parent),
    m_versionModel(new VersionModel(this)),
    m_scoreModel(new FinalScoreModel(this)),
    m_sheetModel(new SheetModel(this)),
    m_isChanged(true)
{
    connect(m_versionModel, &VersionModel::modelChanged, this, &Test::onChange);
    connect(m_scoreModel, &FinalScoreModel::modelChanged, this, &Test::onChange);
    connect(m_sheetModel, &SheetModel::modelChanged, this, &Test::onChange);
}

QString Test::testName() const
{
    return m_testName;
}

void Test::setTestName(const QString &testName)
{
    m_testName = testName;
    m_isChanged = true;
}

VersionModel *Test::versionModel() const
{
    return m_versionModel;
}

FinalScoreModel *Test::scoreModel() const
{
    return m_scoreModel;
}

SheetModel *Test::sheetModel() const
{
    return m_sheetModel;
}

bool Test::load(const QString &filePath)
{
    QFile inFile(filePath);
    if(!inFile.open(QFile::ReadOnly))
    {
        return false;
    }
    // Load the json object.
    QJsonObject testObject = QJsonDocument::fromJson(inFile.readAll()).object();
    inFile.close();
    // Load the data.
    m_testName = testObject.value("Name").toString();
    m_versionModel->fromJson(testObject.value("Versions"));
    m_sheetModel->fromJson(testObject.value("Sheets"));
    m_scoreModel->fromJson(testObject.value("Score"));
    // Save the file path.
    m_filePath = filePath;
    // Remove the changed flag.
    m_isChanged = false;
    return true;
}

void Test::save()
{
    save(m_filePath);
}

void Test::save(const QString &filePath)
{
    // Create a json object.
    QJsonObject testObject;
    // Save the name.
    testObject.insert("Name", m_testName);
    testObject.insert("Versions", m_versionModel->toJson());
    testObject.insert("Sheets", m_sheetModel->toJson());
    testObject.insert("Score", m_scoreModel->toJson());
    // Write the test object data to the file.
    QFile outFile(filePath);
    if(!outFile.open(QFile::WriteOnly))
    {
        return;
    }
    outFile.write(QJsonDocument(testObject).toJson());
    outFile.close();
    // Remove the changed flag.
    m_isChanged = false;
    m_filePath = filePath;
}

void Test::onCompleteDetect(const QString &uniId,
                            const QString &title,
                            const QString &filePath,
                            const QList<QString> &answer)
{
    FinalScoreModel::FinalScore finalScore;
    finalScore.uniId = "u" + uniId;
    finalScore.filePath = filePath;
    if(answer.size()==4)
    {
        finalScore.answer[0] = answer.at(0);
        finalScore.answer[1] = answer.at(1);
        finalScore.answer[2] = answer.at(2);
        finalScore.answer[3] = answer.at(3);
    }
    // Calculate the mark.
    m_versionModel->judge(title, answer,
                          &finalScore.version, &finalScore.finalMark);
    // Add to model
    m_scoreModel->appendMark(finalScore);
    // Data changed.
    m_isChanged = true;
}

void Test::onChange()
{
    m_isChanged = true;
}

QString Test::filePath() const
{
    return m_filePath;
}

void Test::clearScore()
{
    // Clear the score model.
    m_scoreModel->clear();
    // Also update the quiz name of version model.
    m_versionModel->setQuizName(m_testName);
    // Changed.
    m_isChanged = true;
}

bool Test::isChanged() const
{
    return m_isChanged;
}
