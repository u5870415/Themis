#ifndef FINALSCOREMODEL_H
#define FINALSCOREMODEL_H

#include <QAbstractTableModel>

class FinalScoreModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    struct FinalScore
    {
        QString uniId, filePath;
        int version, finalMark;
        QString answer[4];
    };

    enum FinalScoreColumns
    {
        ColumnUniId,
        ColumnVersion,
        ColumnAnswer1,
        ColumnAnswer2,
        ColumnAnswer3,
        ColumnAnswer4,
        ColumnScore,
        FinalScoreColumnCount
    };

    explicit FinalScoreModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;

    int columnCount(const QModelIndex &parent) const override;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;

    QJsonValue toJson();
    bool fromJson(const QJsonValue &json);

    FinalScore getResult(int row) const;

    void appendMark(const FinalScore &result);

signals:
    void modelChanged();

public slots:
    void clear();

private:
    QList<FinalScore> m_results;
};

#endif // FINALSCOREMODEL_H
