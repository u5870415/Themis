#include <QJsonArray>

#include "sheetmodel.h"

SheetModel::SheetModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

int SheetModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_sheets.size();
}

QVariant SheetModel::data(const QModelIndex &index, int role) const
{
    switch(role)
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
        return m_sheets.at(index.row());
    default:
        return QVariant();
    }
}

void SheetModel::appendSheets(const QStringList &sheets)
{
    beginInsertRows(QModelIndex(), m_sheets.size(),
                    m_sheets.size() + sheets.size());
    m_sheets.append(sheets);
    endInsertRows();
    emit modelChanged();
}

bool SheetModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row+count-1);
    for(int i=0; i<count; ++i)
    {
        // remove the row item
        m_sheets.removeAt(row);
    }
    endRemoveRows();
    emit modelChanged();
    return true;
}

QJsonValue SheetModel::toJson()
{
    QJsonArray sheetList;
    for(auto i : m_sheets)
    {
        sheetList.append(i);
    }
    return sheetList;
}

bool SheetModel::fromJson(const QJsonValue &json)
{
    beginResetModel();
    m_sheets = QStringList();
    endResetModel();
    QJsonArray sheetList = json.toArray();
    if(sheetList.isEmpty())
    {
        return true;
    }
    beginInsertRows(QModelIndex(), 0, sheetList.size()-1);
    m_sheets.reserve(sheetList.size());
    for(auto i : sheetList)
    {
        // Add data to list.
        m_sheets.append(i.toString());
    }
    endInsertRows();
    return true;
}

QStringList SheetModel::sheets() const
{
    return m_sheets;
}
