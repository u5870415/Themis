#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

#include "testview.h"
#include "test.h"
#include "themis.h"
#include "paperdetection.h"
#include "configuration.h"
#include "scoredetailview.h"

#include "mainwindow.h"

#include <QDebug>

#define WindowTitle "Themis"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    m_testView(new TestView(this)),
    m_test(nullptr),
    m_testMachine(new Themis()),
    m_createCounter(1),
    m_detailView(new ScoreDetailView(this))
{
    // Initialize the configuration
    Configuration::initial(this);
    PaperDetection::initCharSource();

    // Create menus.
    QMenu *menuFile = createMenu("&File");
    QAction *actNew = createAction("&New test...", QKeySequence::New);
    menuFile->addAction(actNew);
    connect(actNew, &QAction::triggered, this, &MainWindow::onNewTest);
    QAction *actOpen = createAction("&Open test...", QKeySequence::Open);
    menuFile->addAction(actOpen);
    connect(actOpen, &QAction::triggered, this, &MainWindow::onOpenTest);
    QAction *actSave = createAction("&Save", QKeySequence::Save);
    menuFile->addAction(actSave);
    connect(actSave, &QAction::triggered, this, &MainWindow::onSaveTest);
    QAction *actSaveAs = createAction("&Save as...", QKeySequence::SaveAs);
    menuFile->addAction(actSaveAs);
    connect(actSaveAs, &QAction::triggered, this, &MainWindow::onSaveAsTest);
    QAction *actClose = createAction("&Close test", QKeySequence::Close);
    menuFile->addAction(actClose);
    connect(actClose, &QAction::triggered, this, &MainWindow::onClose);
    menuFile->addSeparator();
    QAction *actExit = createAction("E&xit");
    menuFile->addAction(actExit);
    connect(actExit, &QAction::triggered, this, &MainWindow::close);

    QMenu *menuView = createMenu("&View");
    QAction *actShowDetail = createAction("&Paper Detail Window");
    menuView->addAction(actShowDetail);
    connect(actShowDetail, &QAction::triggered,
            [=]
    {
        m_detailView->show();
    });

    QMenu *menuTest = createMenu("&Test");
    QAction *actStart = createAction("&Start marking", QKeySequence(Qt::Key_F5));
    menuTest->addAction(actStart);
    connect(actStart, &QAction::triggered, this, &MainWindow::startTest);
    QAction *actStop = createAction("&Stop marking", QKeySequence(Qt::Key_F6));
    menuTest->addAction(actStop);
    connect(actStop, &QAction::triggered, this, &MainWindow::stopTest);

    // Set the central widget.
    setCentralWidget(m_testView);
    connect(m_testView, &TestView::detailChange,
            m_detailView, &ScoreDetailView::setDetail);
    connect(m_testView, &TestView::detailRemove,
            m_detailView, &ScoreDetailView::clearDetail);

    addDockWidget(Qt::RightDockWidgetArea, m_detailView);
    setDockNestingEnabled(true);

    // Configure the machine.
    m_testMachine->moveToThread(&m_testThread);
    connect(this, &MainWindow::requireStartTest,
            m_testMachine, &Themis::startTesting, Qt::QueuedConnection);
    connect(m_testMachine, &Themis::completeDetect,
            this, &MainWindow::onCompleteDetect, Qt::QueuedConnection);
    m_testThread.start();
}

MainWindow::~MainWindow()
{
    // Stop the test machine.
    m_testMachine->stop();
    // Quit the test thread.
    m_testThread.quit();
    m_testThread.wait();
    m_testMachine->deleteLater();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(!onClose())
    {
        //Ignore the close event.
        event->ignore();
        return;
    }
    // Normally close the window.
    QMainWindow::closeEvent(event);
}

void MainWindow::onNewTest()
{
    // Create a new test.
    if(!onClose())
    {
        return;
    }
    // Create the new test.
    m_test = new Test(this);
    m_test->setTestName(QString("Quiz %1").arg(
                            QString::number(m_createCounter++)));
    setWindowTitle(WindowTitle " - Untitled");
    // Set the test to test view.
    m_testView->setTest(m_test);
}

void MainWindow::onOpenTest()
{
    if(!onClose())
    {
        return;
    }
    // Get the file open.
    QString filePath = QFileDialog::getOpenFileName(this, "Open test", QString(),
                                                    "Themis test file (*.akua)");
    if(!filePath.isEmpty())
    {
        // Load the data.
        m_test = new Test(this);
        if(!m_test->load(filePath))
        {
            //!FIXME: add code here!
            m_test->deleteLater();
            m_test = nullptr;
            return;
        }
        // Set the view.
        m_testView->setTest(m_test);
        setWindowTitle(WindowTitle " - " + m_test->filePath());
    }
}

bool MainWindow::onSaveTest()
{
    if(m_test)
    {
        // Check the file path.
        if(m_test->filePath().isEmpty())
        {
            return onSaveAsTest();
        }
        // Save the test.
        m_test->save();
        return true;
    }
    return false;
}

bool MainWindow::onSaveAsTest()
{
    if(m_test)
    {
        // Get the save as file path.
        QString saveFilePath =
                QFileDialog::getSaveFileName(this, "Save the test as", QString(),
                                             "Themis test file (*.akua)");
        if(saveFilePath.isEmpty())
        {
            return false;
        }
        // Save the test to the file path.
        m_test->save(saveFilePath);
        setWindowTitle(WindowTitle " - " + m_test->filePath());
        // Update the title.
        return true;
    }
    return false;
}

bool MainWindow::onClose()
{
    // Stop your machine first.
    if(m_testMachine->isWorking())
    {
        return false;
    }
    // Create a new test.
    if(m_test)
    {
        // Check is changed.
        if(m_test->isChanged() && !processChange())
        {
            // Cannot move on.
            return false;
        }
        // Clear the original test.
        m_testView->clearTest();
        setWindowTitle(WindowTitle);
        // Delete the test.
        m_test->deleteLater();
        m_test = nullptr;
        return true;
    }
    return true;
}

void MainWindow::startTest()
{
    if(!m_test)
    {
        return;
    }
    // Clear the marking model.
    m_test->clearScore();
    // Set the working sheets.
    m_testMachine->setSheetModel(m_test->sheetModel());
    // Start working.
    emit requireStartTest();
}

void MainWindow::stopTest()
{
    if(!m_test)
    {
        return;
    }
    // Stop test machine.
    m_testMachine->stop();
}

void MainWindow::onCompleteDetect(const QString &uniId, const QString &title,
                                  const QString &filePath,
                                  const QList<QString> &answer)
{
    if(!m_test)
    {
        return;
    }
    // Redirect to test.
    m_test->onCompleteDetect(uniId, title, filePath, answer);
}

bool MainWindow::processChange()
{
    // Allows to move on.
    if(!m_test->isChanged())
    {
        return true;
    }
    // Change detected.
    int saveSelection =
            QMessageBox::question(this, "Save changes",
                          QString("Test %1 is changed, save the changes?").arg(
                                      m_test->testName()),
                          QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    switch(saveSelection)
    {
    case QMessageBox::Yes:
        return onSaveTest();
    case QMessageBox::No:
        return true;
    case QMessageBox::Cancel:
        return false;
    default:
        return true;
    }
}

inline QMenu *MainWindow::createMenu(const QString &caption)
{
    QMenu *menu = new QMenu(caption, this);
    menuBar()->addMenu(menu);
    return menu;
}

inline QAction *MainWindow::createAction(const QString &title,
                                         const QKeySequence &shortcut)
{
    QAction *action = new QAction(title, this);
    if(!shortcut.isEmpty())
    {
        action->setShortcut(shortcut);
    }
    return action;
}
