#include <QDesktopServices>
#include <QUrl>

#include "clickablelabel.h"

ClickableLabel::ClickableLabel(QWidget *parent) : QLabel(parent)
{

}

void ClickableLabel::mouseDoubleClickEvent(QMouseEvent *event)
{
    QLabel::mouseDoubleClickEvent(event);
    if(text().isEmpty())
    {
        return;
    }
    QDesktopServices::openUrl(QUrl::fromLocalFile(text()));
}
