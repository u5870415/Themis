#ifndef TESTVIEW_H
#define TESTVIEW_H

#include <QSplitter>

class QTreeView;
class QLabel;
class QListView;
class QLineEdit;
class QAbstractItemView;
class QAbstractItemModel;
class FinalScoreView;
class Test;
class SheetModel;
class VersionModel;
class FinalScoreModel;
class TestView : public QSplitter
{
    Q_OBJECT
public:
    explicit TestView(QWidget *parent = nullptr);

signals:
    void detailRemove();
    void detailChange(const QString &uniId, const QString &version,
                      const QString &filePath,
                      const QString &ans0, const QString &ansc0,
                      const QString &ans1, const QString &ansc1,
                      const QString &ans2, const QString &ansc2,
                      const QString &ans3, const QString &ansc3,
                      const QString &score);

public slots:
    void addSheet();
    void removeSheet();
    void addVersion();
    void removeVersion();

    void setTest(Test *test);
    void clearTest();

private slots:
    void onSheetsCountChange();

private:
    inline void removeRowsFromModel(QAbstractItemView *view,
                          QAbstractItemModel *model);
    QLineEdit *m_testName;
    FinalScoreView *m_scoreView;
    QLabel *m_sheetsStatus;
    QListView *m_sheetsView;
    QTreeView *m_versionView;

    Test *m_test;
    VersionModel *m_versionModel;
    FinalScoreModel *m_scoreModel;
    SheetModel *m_sheetModel;
};

#endif // TESTVIEW_H
