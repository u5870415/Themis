#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QObject>

#define configure (Configuration::instance())

class Configuration : public QObject
{
    Q_OBJECT
public:
    static Configuration *instance();
    static void initial(QObject *parent = nullptr);

    int grayThreshold() const;
    void setGrayThreshold(int grayThreshold);

signals:

public slots:

private:
    static Configuration *m_instance;
    explicit Configuration(QObject *parent = nullptr);

    int m_grayThreshold;
};

#endif // CONFIGURATION_H
