#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>

class ClickableLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ClickableLabel(QWidget *parent = nullptr);

signals:

public slots:

protected:
    void mouseDoubleClickEvent(QMouseEvent *event) override;
};

#endif // CLICKABLELABEL_H
