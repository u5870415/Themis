#include <QJsonObject>
#include <QJsonArray>

#include "finalscoremodel.h"

FinalScoreModel::FinalScoreModel(QObject *parent) :
    QAbstractTableModel(parent)
{

}

int FinalScoreModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_results.size();
}

int FinalScoreModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return FinalScoreColumnCount;
}

QVariant FinalScoreModel::headerData(int section, Qt::Orientation orientation,
                                     int role) const
{
    if(orientation==Qt::Vertical)
    {
        return QVariant();
    }
    switch(role)
    {
    case Qt::DisplayRole:
        switch(section)
        {
        case ColumnUniId:
            return "Uni ID";
        case ColumnVersion:
            return "Version";
        case ColumnAnswer1:
            return "Answer 1";
        case ColumnAnswer2:
            return "Answer 2";
        case ColumnAnswer3:
            return "Answer 3";
        case ColumnAnswer4:
            return "Answer 4";
        case ColumnScore:
            return "Final Mark";
        default:
            return QVariant();
        }
    default:
        return QVariant();
    }
}

QVariant FinalScoreModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
        return QVariant();
    }
    const FinalScore &score = m_results.at(index.row());
    switch(role)
    {
    case Qt::DisplayRole:
        switch(index.column())
        {
        case ColumnUniId:
            return score.uniId;
        case ColumnVersion:
            return QString::number(score.version);
        case ColumnAnswer1:
            return score.answer[0];
        case ColumnAnswer2:
            return score.answer[1];
        case ColumnAnswer3:
            return score.answer[2];
        case ColumnAnswer4:
            return score.answer[3];
        case ColumnScore:
            return score.finalMark;
        default:
            return QVariant();
        }
    default:
        return QVariant();
    }
}

QJsonValue FinalScoreModel::toJson()
{
    QJsonArray scoreList;
    for(auto i : m_results)
    {
        QJsonObject result;
        result.insert("uniId", i.uniId);
        result.insert("filePath", i.filePath);
        result.insert("version", i.version);
        result.insert("finalMark", i.finalMark);
        QJsonArray resultAns;
        resultAns.append(i.answer[0]);
        resultAns.append(i.answer[1]);
        resultAns.append(i.answer[2]);
        resultAns.append(i.answer[3]);
        result.insert("answer", resultAns);
        // Add to list.
        scoreList.append(result);
    }
    return scoreList;
}

bool FinalScoreModel::fromJson(const QJsonValue &json)
{
    clear();
    QJsonArray scoreList = json.toArray();
    if(scoreList.isEmpty())
    {
        return false;
    }
    beginInsertRows(QModelIndex(), 0, scoreList.size()-1);
    m_results.reserve(scoreList.size());
    for(auto i : scoreList)
    {
        QJsonObject result = i.toObject();
        FinalScore item;
        item.uniId = result.value("uniId").toString();
        item.filePath = result.value("filePath").toString();
        item.version = result.value("version").toInt();
        item.finalMark = result.value("finalMark").toInt();
        QJsonArray resultAns = result.value("answer").toArray();
        item.answer[0] = resultAns.at(0).toString();
        item.answer[1] = resultAns.at(1).toString();
        item.answer[2] = resultAns.at(2).toString();
        item.answer[3] = resultAns.at(3).toString();
        // Add to list.
        m_results.append(item);
    }
    endInsertRows();
    return true;
}

FinalScoreModel::FinalScore FinalScoreModel::getResult(int row) const
{
    return m_results.at(row);
}

void FinalScoreModel::appendMark(const FinalScoreModel::FinalScore &result)
{
    beginInsertRows(QModelIndex(), m_results.size(), m_results.size());
    m_results.append(result);
    endInsertRows();
}

void FinalScoreModel::clear()
{
    beginResetModel();
    m_results = QList<FinalScore>();
    endResetModel();
}
