#include <QLabel>
#include <QPushButton>
#include <QFileDialog>
#include <QBoxLayout>
#include <QLineEdit>
#include <QTabWidget>
#include <QHeaderView>
#include <QListView>
#include <QTreeView>

#include "finalscoreview.h"
#include "finalscoremodel.h"
#include "sheetmodel.h"
#include "versionmodel.h"
#include "test.h"

#include "testview.h"

#include <QDebug>

TestView::TestView(QWidget *parent) :
    QSplitter(Qt::Horizontal, parent),
    m_testName(new QLineEdit(this)),
    m_scoreView(new FinalScoreView(this)),
    m_sheetsStatus(new QLabel(this)),
    m_sheetsView(new QListView(this)),
    m_versionView(new QTreeView(this)),
    m_test(nullptr)
{
    setContentsMargins(0, 0, 0, 0);
    QWidget *infoContainer = new QWidget(this);
    addWidget(infoContainer);

    QBoxLayout *containerLayout = new QBoxLayout(QBoxLayout::TopToBottom,
                                                 infoContainer);
    containerLayout->addWidget(new QLabel("Quiz Title", this));
    containerLayout->addWidget(m_testName);
    QTabWidget *testContent = new QTabWidget(this);
    testContent->setMinimumWidth(340);
    testContent->setContentsMargins(0, 0, 0, 0);
    containerLayout->addWidget(testContent, 1);

    // Versions.
    QWidget *versionContainer = new QWidget(this);
    QBoxLayout *versionLayout = new QBoxLayout(QBoxLayout::TopToBottom,
                                               versionContainer);
    versionContainer->setLayout(versionLayout);
    QBoxLayout *versionEditors = new QBoxLayout(QBoxLayout::LeftToRight,
                                                nullptr);
    versionLayout->addLayout(versionEditors);
    QPushButton *addVersion = new QPushButton(this);
    addVersion->setToolTip("Create a new version");
    addVersion->setIcon(QIcon(":/icon/img/add.png"));
    versionEditors->addWidget(addVersion);
    connect(addVersion, &QPushButton::clicked, this, &TestView::addVersion);
    QPushButton *removeVersion = new QPushButton(this);
    removeVersion->setToolTip("Remove the selected version");
    removeVersion->setIcon(QIcon(":/icon/img/remove.png"));
    versionEditors->addWidget(removeVersion);
    connect(removeVersion, &QPushButton::clicked, this, &TestView::removeVersion);
    versionEditors->addStretch();
    versionLayout->addWidget(m_versionView);
    testContent->addTab(versionContainer, "Versions");

    // Sheets
    QWidget *sheetsContainer = new QWidget(this);
    QBoxLayout *sheetsLayout = new QBoxLayout(QBoxLayout::TopToBottom,
                                              sheetsContainer);
    sheetsContainer->setLayout(sheetsLayout);
    QBoxLayout *sheetsEditors = new QBoxLayout(QBoxLayout::LeftToRight,
                                               nullptr);
    sheetsLayout->addLayout(sheetsEditors);

    QPushButton *addSheets = new QPushButton(this);
    addSheets->setToolTip("Add sheets");
    addSheets->setIcon(QIcon(":/icon/img/add.png"));
    sheetsEditors->addWidget(addSheets);
    connect(addSheets, &QPushButton::clicked, this, &TestView::addSheet);

    QPushButton *removeSheets = new QPushButton(this);
    removeSheets->setToolTip("Remove selected sheets");
    sheetsEditors->addWidget(removeSheets);
    removeSheets->setIcon(QIcon(":/icon/img/remove.png"));
    connect(removeSheets, &QPushButton::clicked, this, &TestView::removeSheet);
    sheetsEditors->addWidget(m_sheetsStatus, 1);
    m_sheetsView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    sheetsLayout->addWidget(m_sheetsView, 1);
    testContent->addTab(sheetsContainer, "Sheets");
    //Add the score view.
    QWidget *scoreContainer = new QWidget(this);
    QBoxLayout *scoreLayout = new QBoxLayout(QBoxLayout::TopToBottom,
                                             scoreContainer);
    scoreContainer->setLayout(scoreLayout);
    scoreLayout->addWidget(m_scoreView);
    m_scoreView->setIndentation(0);
    addWidget(scoreContainer);

    // Configure the versions.
    setStretchFactor(0, 0);
    setStretchFactor(1, 1);
}

void TestView::onSheetsCountChange()
{
    if(m_sheetModel)
    {
        m_sheetsStatus->setText(QString("Total Sheets: %1").arg(
                                    QString::number(m_sheetModel->rowCount())));
    }
}

void TestView::removeRowsFromModel(QAbstractItemView *view, QAbstractItemModel *model)
{
    // Get the selection from the view.
    QModelIndexList selectedItems =
            view->selectionModel()->selectedRows();
    if(selectedItems.isEmpty())
    {
        return;
    }
    QList<int> rows;
    for(auto itemIndex : selectedItems)
    {
        rows.append(itemIndex.row());
    }
    // Sort the rows.
    std::sort(rows.begin(), rows.end());
    // Get the data.
    while(!rows.isEmpty())
    {
        model->removeRow(rows.takeLast());
    }
}

void TestView::addSheet()
{
    if(m_sheetModel)
    {
        QStringList sheetFiles
                = QFileDialog::getOpenFileNames(this, "Add quiz sheets", QString(),
                                                "Image Files (*.png *.jpg)");
        if(sheetFiles.isEmpty())
        {
            return;
        }
        // Add sheet files.
        m_sheetModel->appendSheets(sheetFiles);
    }
}

void TestView::removeSheet()
{
    if(m_sheetModel)
    {
        removeRowsFromModel(m_sheetsView, m_sheetModel);
    }
}

void TestView::addVersion()
{
    if(m_versionModel)
    {
        m_versionModel->appendVersion();
        m_versionView->selectionModel()->select(
                    m_versionModel->index(m_versionModel->rowCount()-1,
                                          1),
                    QItemSelectionModel::Rows | QItemSelectionModel::ClearAndSelect);
    }
}

void TestView::removeVersion()
{
    if(m_versionModel)
    {
        removeRowsFromModel(m_versionView, m_versionModel);
    }
}

void TestView::setTest(Test *test)
{
    // Save the test.
    if(m_test)
    {
        return;
    }
    m_test = test;

    // Set the name.
    m_testName->setText(test->testName());
    connect(m_testName, &QLineEdit::textChanged, m_test, &Test::setTestName);

    // Get the model.
    m_scoreModel = test->scoreModel();
    m_sheetModel = test->sheetModel();
    m_versionModel = test->versionModel();

    // Configure the view.
    m_scoreView->setModel(m_scoreModel);
    m_sheetsView->setModel(m_sheetModel);
    connect(m_sheetModel, &SheetModel::rowsInserted,
            this, &TestView::onSheetsCountChange);
    connect(m_sheetModel, &SheetModel::rowsRemoved,
            this, &TestView::onSheetsCountChange);
    onSheetsCountChange();
    m_versionView->setModel(m_versionModel);
    m_versionView->setIndentation(0);
    m_versionView->header()->setStretchLastSection(false);
    m_versionView->header()->resizeSections(QHeaderView::ResizeToContents);

    // Move to the test name editing.
    m_testName->setFocus();
    m_testName->setCursorPosition(m_testName->text().length());

    connect(m_scoreView->selectionModel(), &QItemSelectionModel::currentChanged,
            [=]
    {
        if(m_scoreView->currentIndex().isValid())
        {
            // Get the item.
            FinalScoreModel::FinalScore result =
                    m_scoreModel->getResult(m_scoreView->currentIndex().row());
            VersionModel::Versions version =
                    m_versionModel->getVersion(result.version-1);
            emit detailChange(result.uniId, QString::number(result.version),
                              result.filePath,
                              result.answer[0], version.answer[0],
                              result.answer[1], version.answer[1],
                              result.answer[2], version.answer[2],
                              result.answer[3], version.answer[3],
                              QString::number(result.finalMark));
        }
    });
}

void TestView::clearTest()
{
    if(!m_test)
    {
        return;
    }

    emit detailRemove();
    disconnect(m_sheetModel, 0, 0, 0);
    disconnect(m_testName, 0, 0, 0);

    m_testName->clear();

    m_scoreModel = nullptr;
    m_sheetModel = nullptr;
    m_versionModel = nullptr;

    m_scoreView->setModel(nullptr);
    m_sheetsView->setModel(nullptr);
    m_versionView->setModel(nullptr);

    m_test = nullptr;
}
