#include <QPixmap>
#include <QImage>
#include <QPainter>

#include "paperdetection.h"
#include "configuration.h"

#define DETECT_LETTER_SIZE 31

#include <QDebug>

QList<QImage> PaperDetection::m_itemSource=QList<QImage>();
QRgb PaperDetection::m_blackPixel = qRgb(0, 0, 0);
QImage PaperDetection::debugImg=QImage();

PaperDetection::PaperDetection(QObject *parent) :
    QObject(parent)
{
}

void PaperDetection::initCharSource()
{
    // Resize the source.
    m_itemSource.reserve(62);
    // Append the data.
    QPixmap itemCache(50, 50);
    QPainter painter;
    QFont targetFont;
    targetFont.setFamily("Arial");
    targetFont.setPixelSize(40);
    // Caps letter.
    for(int i=0; i<26; ++i)
    {
        painter.begin(&itemCache);
        painter.setRenderHints(QPainter::Antialiasing |
                               QPainter::TextAntialiasing |
                               QPainter::SmoothPixmapTransform);
        painter.setFont(targetFont);
        // Draw the background
        painter.fillRect(itemCache.rect(), Qt::white);
        // Draw the item.
        painter.drawText(itemCache.rect(), Qt::AlignCenter,
                         QString(QChar('A'+i)));
        painter.end();
        // Scale the data.
        QImage item = PaperDetection::binarize(itemCache.toImage());
        item = item.copy(PaperDetection::getBorder(
                             PaperDetection::findContours(item),
                             item.width(),
                             item.height()));
        m_itemSource.append(PaperDetection::binarize(
                                item.scaled(DETECT_LETTER_SIZE,
                                            DETECT_LETTER_SIZE)));
    }
    // Normal letter.
    for(int i=0; i<26; ++i)
    {
        painter.begin(&itemCache);
        painter.setRenderHints(QPainter::Antialiasing |
                               QPainter::TextAntialiasing |
                               QPainter::SmoothPixmapTransform);
        painter.setFont(targetFont);
        // Draw the background
        painter.fillRect(itemCache.rect(), Qt::white);
        // Draw the item.
        painter.drawText(itemCache.rect(), Qt::AlignCenter,
                         QString(QChar('a'+i)));
        painter.end();
        // Scale the data.
        QImage item = PaperDetection::binarize(itemCache.toImage());
        item = item.copy(PaperDetection::getBorder(
                             PaperDetection::findContours(item),
                             item.width(),
                             item.height()));
        m_itemSource.append(PaperDetection::binarize(
                                item.scaled(DETECT_LETTER_SIZE,
                                            DETECT_LETTER_SIZE)));
    }
    // Numbers.
    for(int i=0; i<10; ++i)
    {
        painter.begin(&itemCache);
        painter.setRenderHints(QPainter::Antialiasing |
                               QPainter::TextAntialiasing |
                               QPainter::SmoothPixmapTransform);
        painter.setFont(targetFont);
        // Draw the background
        painter.fillRect(itemCache.rect(), Qt::white);
        // Draw the item.
        painter.drawText(itemCache.rect(), Qt::AlignCenter,
                         QString(QChar('0'+i)));
        painter.end();
        // Scale the data.
        QImage item = PaperDetection::binarize(itemCache.toImage());
        item = item.copy(PaperDetection::getBorder(
                             PaperDetection::findContours(item),
                             item.width(),
                             item.height()));
        m_itemSource.append(PaperDetection::binarize(
                                item.scaled(DETECT_LETTER_SIZE,
                                            DETECT_LETTER_SIZE)));
    }
}

QImage PaperDetection::getCharSource(int index)
{
    return m_itemSource.at(index);
}

QImage PaperDetection::binarize(const QImage &original)
{
    // Transform the image into mono.
    QImage binaryMap(original.width(), original.height(), QImage::Format_Mono);
    const int threshold = configure->grayThreshold();
    // Loop and detect all the images.
    for(int i=0; i<original.width(); ++i)
    {
        for(int j=0; j<original.height(); ++j)
        {
            // Check the gray.
            binaryMap.setPixel(i, j,
                               (qGray(original.pixel(i, j)) > threshold) ?
                                   1 : 0);
        }
    }
    // Give back the binary map.
    return binaryMap;
}

QList<QPoint> PaperDetection::findContours(const QImage &binImage)
{
    QList<QPoint> borderPoints;
    if(binImage.format() != QImage::Format_Mono)
    {
        // incorrect type.
        return borderPoints;
    }
    // Start to search the left point.
    const int imgWidth = binImage.width(), imgHeight = binImage.height();
    bool alreadyFound = false;
    for(int i=0; i<imgWidth; ++i)
    {
        for(int j=0; j<imgHeight; ++j)
        {
            // Find the black pixel.
            if(binImage.pixel(i, j) == m_blackPixel)
            {
                // Find the left most point.
                borderPoints.append(QPoint(i, j));
                alreadyFound = true;
                break;
            }
        }
        if(alreadyFound)
        {
            break;
        }
    }
    // Start to search the right point.
    alreadyFound = false;
    for(int i=imgWidth-1; i>-1; --i)
    {
        for(int j=0; j<imgHeight; ++j)
        {
            // Find the black pixel.
            if(binImage.pixel(i, j) == m_blackPixel)
            {
                // Find the left most point.
                borderPoints.append(QPoint(i, j));
                alreadyFound = true;
                break;
            }
        }
        if(alreadyFound)
        {
            break;
        }
    }
    // Start to search tht top point.
    alreadyFound = false;
    for(int j=0; j<imgHeight; ++j)
    {
        for(int i=0; i<imgWidth; ++i)
        {
            // Find the black pixel.
            if(binImage.pixel(i, j) == m_blackPixel)
            {
                // Find the left most point.
                borderPoints.append(QPoint(i, j));
                alreadyFound = true;
                break;
            }
        }
        if(alreadyFound)
        {
            break;
        }
    }
    // Start to search tht bottom point.
    alreadyFound = false;
    for(int j=imgHeight-1; j>-1; --j)
    {
        for(int i=0; i<imgWidth; ++i)
        {
            // Find the black pixel.
            if(binImage.pixel(i, j) == m_blackPixel)
            {
                // Find the left most point.
                borderPoints.append(QPoint(i, j));
                alreadyFound = true;
                break;
            }
        }
        if(alreadyFound)
        {
            break;
        }
    }
    // Give back the point list.
    return borderPoints;
}

bool PaperDetection::correctHorizontal(QImage &binImage)
{
    // Sort the points by height.
    QList<QPoint> borderPoints = PaperDetection::findContours(binImage);
    std::sort(borderPoints.begin(), borderPoints.end(),
              [](const QPoint &x, const QPoint &y)
              {
                  return x.y() < y.y();
              });
    if(borderPoints.size()!=4)
    {
        // Something wrong.
        return false;
    }
    // Get the first two points.
    QPoint corner1 = borderPoints.at(0), corner2 = borderPoints.at(1);
    if(corner2.x() < corner1.x())
    {
        // Swap the points.
        QPoint tempPoint = corner1;
        corner1 = corner2;
        corner2 = tempPoint;
    }
    // Get the two border.
    int triHeight = qAbs(corner1.y() - corner2.y()),
        triWidth = qAbs(corner2.x() - corner1.x());
    if(triHeight!=0)
    {
        // Calculate the spin angle.
        double angle = atan2(triHeight, triWidth) / M_PI * 180.0f;
        // Check the rotate direction.
        QTransform spinTransform;
        if(corner1.y() > corner2.y())
        {
            // Spin counter clockwise to correct.
            spinTransform.rotate(angle);
        }
        else
        {
            spinTransform.rotate(-1.0*angle);
        }
        // Apply the transformed
        binImage = binImage.transformed(spinTransform, Qt::SmoothTransformation);
        {
            QPixmap cache(binImage.width(), binImage.height());
            cache.fill(Qt::white);
            QPainter painter(&cache);
            painter.setRenderHints(QPainter::Antialiasing |
                                   QPainter::SmoothPixmapTransform);
            painter.drawPixmap(0, 0, QPixmap::fromImage(binImage));
            painter.end();
            binImage = cache.toImage();
        }
        binImage = PaperDetection::binarize(binImage);
    }
    // Detect the new border.
    borderPoints = PaperDetection::findContours(binImage);
    // Clip the image.
    binImage = binImage.copy(getBorder(borderPoints,
                                       binImage.width(), binImage.height()));
    // Rescaling the image.
    binImage = binImage.scaled(976, 1465,
                               Qt::IgnoreAspectRatio,
                               Qt::SmoothTransformation);
    binImage = PaperDetection::binarize(binImage);
    return true;
}

QString PaperDetection::readTitle(const QImage &correctImage)
{
    // Clip the title
    QImage titleArea = correctImage.copy(20, 20, 414, 123);
    QList<QPoint> borders = PaperDetection::findContours(titleArea);
    // Split each letter and identify all the data.
    titleArea = titleArea.copy(getBorder(borders,
                                         titleArea.width(),
                                         titleArea.height()));
    // Detect the column is white.
    QString titleText;
    int lastEmptyColumn=0, blackCounter;
    for(int i=0; i<titleArea.width(); ++i)
    {
        blackCounter = 0;
        for(int j=0; j<titleArea.height(); ++j)
        {
            blackCounter += (titleArea.pixel(i, j) == m_blackPixel);
        }
        // Check the black counter.
        if(blackCounter < 2)
        {
            // For 0 or 1 pixel, treat as empty item.
            if(lastEmptyColumn!=i-1)
            {
                // Detect one letter.
                titleText.append(detectChar(
                                     titleArea.copy(lastEmptyColumn, 0,
                                                    i-lastEmptyColumn,
                                                    titleArea.height())));
            }
            // Update the last empty column.
            lastEmptyColumn = i;
        }
    }
    // Add the last char.
    titleText.append(detectChar(
                         titleArea.copy(lastEmptyColumn, 0,
                                        titleArea.width()-lastEmptyColumn,
                                        titleArea.height())));
    return titleText.toLower();
}

QString PaperDetection::readUniId(const QImage &correctImage)
{
    // Split each letter and identify all the data.
    QImage uniArea = correctImage.copy(485, 100, 400, 265);
    // Split for the first list.
    static const int hSplit[8] = {0, 58, 114, 174, 230, 285, 340, 393};
    char uniId[8];
    uniId[7]='\0';
    for(int i=0; i<7; ++i)
    {
        // Clip the image.
        QImage columnArea= uniArea.copy(hSplit[i], 0,
                                        hSplit[i+1]-hSplit[i],
                                        uniArea.height());
        // Detect the number of the column area.
        QList<QPoint> corners = PaperDetection::findContours(columnArea);
        uniId[i] = readUniColumn(
                    columnArea.copy(getBorder(corners,
                                              columnArea.width(),
                                              columnArea.height())));
    }
    return QString::fromLatin1(uniId);
}

QList<QString> PaperDetection::readAnswer(const QImage &correctImage)
{
    QImage answerArea = correctImage.copy(565, 380, 350, 950);
    static const int vSplit[5] = {0, 220, 470, 730, 950};
    QString answer[4];
    // Split the answer area and detect the answer.
    for(int i=0; i<4; ++i)
    {
        QImage answerItem = answerArea.copy(0, vSplit[i],
                                            answerArea.width(),
                                            vSplit[i+1]-vSplit[i]);
        // Detect the alrea.
        QList<QPoint> corners = PaperDetection::findContours(answerItem);
        answerItem = answerItem.copy(getBorder(corners,
                                               answerItem.width(),
                                               answerItem.height()));
        // Get the answer items.
        answer[i] = readAnswerItem(answerItem);
    }
    // Translate the answer array into list.
    QList<QString> result;
    result.append(answer[0]);
    result.append(answer[1]);
    result.append(answer[2]);
    result.append(answer[3]);
    return result;
}

char PaperDetection::detectChar(QImage itemImg)
{
    itemImg = itemImg.copy(getBorder(findContours(itemImg),
                                     itemImg.width(),
                                     itemImg.height()));
    // Scale to letter size.
    itemImg = binarize(itemImg.scaled(DETECT_LETTER_SIZE,
                                      DETECT_LETTER_SIZE,
                                      Qt::IgnoreAspectRatio,
                                      Qt::SmoothTransformation));

    int maxItem = -1, maxItemIndex = -1;
    for(int i=0; i<m_itemSource.size(); ++i)
    {
        // Calculate the similarity of the item and the source.
        int samePixelCount = samePixels(itemImg, m_itemSource.at(i));
        if(samePixelCount>maxItem)
        {
            maxItem = samePixelCount;
            maxItemIndex = i;
        }
    }
    if(maxItemIndex < 26)
    {
        return 'A' + maxItemIndex;
    }
    else if(maxItemIndex < 52)
    {
        return 'a' + maxItemIndex - 26;
    }
    return '0' + maxItemIndex - 52;
}

int PaperDetection::samePixels(const QImage &target, const QImage &source)
{
    if(target.size() != source.size())
    {
        // Incorrect size.
        return 0.0f;
    }
    int counter = 0;
    for(int i=0; i<target.width(); ++i)
    {
        for(int j=0; j<target.height(); ++j)
        {
            counter += (target.pixel(i, j)==source.pixel(i, j));
        }
    }
    return counter;
}

char PaperDetection::readUniColumn(const QImage &correctColumn)
{
    static const int vSplit[11] = {0,  21,  47,  71,  97, 123,
                                   148, 173, 200, 226, 266};
    for(int i=0; i<10; ++i)
    {
        // Clip the area from the image.
        QImage rowArea = correctColumn.copy(0, vSplit[i],
                                            correctColumn.width(),
                                            vSplit[i+1]-vSplit[i]);
        rowArea = rowArea.copy(getBorder(PaperDetection::findContours(rowArea),
                                         rowArea.width(),
                                         rowArea.height()));
        if(PaperDetection::isSelected(rowArea))
        {
            // Find the selected number.
            return '0' + i;
        }
    }
    return '\0';
}

QString PaperDetection::readAnswerItem(const QImage &answerItem)
{
    // Get the item area.
    QImage items = answerItem.copy(0, 48, 45, answerItem.height() - 48);
    static const int vSplit[5] = {0, 30, 65, 100, 126};
    QString answer;
    // Split the items and detect each of them.
    for(int i=0; i<4; ++i)
    {
        // CLip the area from the image.
        QImage itemArea;
        if(i==3)
        {
            itemArea = items.copy(0, vSplit[i],
                                  items.width(), items.height()-vSplit[i]);
        }
        else
        {
           itemArea = items.copy(0, vSplit[i],
                                 items.width(), vSplit[i+1]-vSplit[i]);
        }
        itemArea = itemArea.copy(getBorder(
                                     PaperDetection::findContours(itemArea),
                                     itemArea.width(),
                                     itemArea.height()));
        debugImg = itemArea;
        int counter;
        if(PaperDetection::isSelected(itemArea, &counter))
        {
            // Find the result;
            answer.append(QChar('A' + i));
        }
    }
    return answer;
}

bool PaperDetection::isSelected(const QImage &img, int *count)
{
    const int imgW = img.width(), imgH = img.height();
    int counter = 0;
    for(int i=0; i<imgW; ++i)
    {
        for(int j=0; j<imgH; ++j)
        {
            counter += (img.pixel(i, j) == m_blackPixel);
        }
    }
    const int imgSize = (imgW * imgH);
    // Larger than 3/4 treat as selected.
    if(count)
    {
        // Output the counter.
        *count = counter;
    }
    return counter > (imgSize>>1);
}

QRect PaperDetection::getBorder(const QList<QPoint> &borderPoints,
                                int entireWidth, int entireHeight)
{
    int imgX=entireWidth, imgY=entireHeight,
        imgWidth=-1, imgHeight=-1;
    if(borderPoints.size() != 4)
    {
        qDebug()<<"border points size incorrect.";
        return QRect();
    }
    for(int i=0; i<4; ++i)
    {
        imgX = qMin(imgX, borderPoints.at(i).x());
        imgY = qMin(imgY, borderPoints.at(i).y());
        imgWidth = qMax(imgWidth, borderPoints.at(i).x());
        imgHeight = qMax(imgHeight, borderPoints.at(i).y());
    }
    imgWidth -= imgX; imgHeight -= imgY;
    // Give back the border.
    return QRect(imgX, imgY, imgWidth, imgHeight);
}

