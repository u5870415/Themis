#ifndef VERSIONMODEL_H
#define VERSIONMODEL_H

#include <QAbstractTableModel>

class VersionModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    struct Versions
    {
        QString answer[4];
    };

    explicit VersionModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;

    void appendVersion();

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    QJsonValue toJson();
    bool fromJson(const QJsonValue &json);

    void setQuizName(const QString &quizName);

    Versions getVersion(int row) const;

signals:
    void modelChanged();

public slots:
    bool judge(const QString &title, const QList<QString> &answer,
               int *matchedIndex, int *score);

private:
    int similarity(QString string1, QString string2);
    QStringList m_versionNames;
    QList<Versions> m_versions;
};

#endif // VERSIONMODEL_H
