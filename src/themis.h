#ifndef THEMIS_H
#define THEMIS_H

#include <QObject>

class SheetModel;
class Themis : public QObject
{
    Q_OBJECT
public:
    explicit Themis(QObject *parent = nullptr);

    bool isWorking() const;
    void stop();
    void setSheetModel(SheetModel *sheetModel);

signals:
    void completeDetect(const QString &uniId,
                        const QString &title,
                        const QString &filePath,
                        const QList<QString> &answer);
    void finish();

public slots:
    void startTesting();

private:
    inline void reset();
    QStringList m_candidates;
    bool m_stop, m_working;
};

#endif // THEMIS_H
