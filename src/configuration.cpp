#include "configuration.h"

Configuration *Configuration::m_instance = nullptr;

Configuration *Configuration::instance()
{
    return m_instance;
}

void Configuration::initial(QObject *parent)
{
    if(m_instance)
    {
        return;
    }
    // Create the global instance.
    m_instance = new Configuration(parent);
}

Configuration::Configuration(QObject *parent) : QObject(parent),
    m_grayThreshold(150)
{
}

int Configuration::grayThreshold() const
{
    return m_grayThreshold;
}

void Configuration::setGrayThreshold(int grayThreshold)
{
    m_grayThreshold = grayThreshold;
}
