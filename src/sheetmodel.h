#ifndef SHEETMODEL_H
#define SHEETMODEL_H

#include <QAbstractListModel>

class SheetModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit SheetModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    void appendSheets(const QStringList &sheets);

    bool removeRows(int row, int count,
                    const QModelIndex &parent = QModelIndex()) override;

    QJsonValue toJson();
    bool fromJson(const QJsonValue &json);

    QStringList sheets() const;

signals:
    void modelChanged();

public slots:

private:
    QStringList m_sheets;
};

#endif // SHEETMODEL_H
