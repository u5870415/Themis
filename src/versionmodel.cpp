#include <QJsonArray>

#include "versionmodel.h"

#include <QDebug>

VersionModel::VersionModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

int VersionModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_versions.size();
}

int VersionModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 5;
}

QVariant VersionModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
        return QVariant();
    }
    const Versions &version = m_versions.at(index.row());
    switch(role)
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
        switch(index.column())
        {
        case 0:
            return "V" + QString::number(index.row()+1);
        case 1:
            return version.answer[0];
        case 2:
            return version.answer[1];
        case 3:
            return version.answer[2];
        case 4:
            return version.answer[3];
        default:
            return QVariant();
        }
    default:
        return QVariant();
    }
}

bool VersionModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid())
    {
        return false;
    }
    // Preprocess the string.
    Versions version = m_versions.at(index.row());
    switch(role)
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
    {
        QString formatAnswer;
        {
            QString answerValue = value.toString().toUpper();
            QHash<QChar, bool> answerMap;
            for(auto i : answerValue)
            {
                if(i.isLetter())
                {
                    answerMap.insert(i, true);
                }
            }
            QList<QChar> answerKeys = answerMap.keys();
            for(auto i : answerKeys)
            {
                formatAnswer.append(i);
            }
            std::sort(formatAnswer.begin(), formatAnswer.end());
        }
        // Get all the answer.
        switch(index.column())
        {
        case 1:
            version.answer[0] = formatAnswer;
            break;
        case 2:
            version.answer[1] = formatAnswer;
            break;
        case 3:
            version.answer[2] = formatAnswer;
            break;
        case 4:
            version.answer[3] = formatAnswer;
            break;
        default:
            return false;
        }
        m_versions.replace(index.row(), version);
        emit modelChanged();
        return true;
    }
    default:
        return false;
    }
}

QVariant VersionModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Vertical)
    {
        return QVariant();
    }
    switch(role)
    {
    case Qt::DisplayRole:
        switch(section)
        {
        case 0:
            return "Version";
        case 1:
            return "Answer 1";
        case 2:
            return "Answer 2";
        case 3:
            return "Answer 3";
        case 4:
            return "Answer 4";
        }
    default:
        return QVariant();
    }
}

Qt::ItemFlags VersionModel::flags(const QModelIndex &index) const
{
    if(index.column()>0)
    {
        return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    }
    return QAbstractTableModel::flags(index);
}

void VersionModel::appendVersion()
{
    beginInsertRows(QModelIndex(), m_versions.size(), m_versions.size());
    Versions emptyVersion;
    m_versions.append(emptyVersion);
    endInsertRows();
    emit modelChanged();
}

bool VersionModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row+count-1);
    for(int i=0; i<count; ++i)
    {
        m_versions.removeAt(row);
    }
    endRemoveRows();
    emit modelChanged();
    return true;
}

QJsonValue VersionModel::toJson()
{
    QJsonArray versionList;
    for(auto i : m_versions)
    {
        // Backup the array.
        QJsonArray versionData;
        versionData.append(i.answer[0]);
        versionData.append(i.answer[1]);
        versionData.append(i.answer[2]);
        versionData.append(i.answer[3]);
        // Add data to list.
        versionList.append(versionData);
    }
    // Give back the list.
    return versionList;
}

bool VersionModel::fromJson(const QJsonValue &json)
{
    beginResetModel();
    m_versions = QList<Versions>();
    endResetModel();
    QJsonArray versionList = json.toArray();
    if(versionList.isEmpty())
    {
        return true;
    }
    beginInsertRows(QModelIndex(), 0, versionList.size()-1);
    m_versions.reserve(versionList.size());
    for(auto i : versionList)
    {
        QJsonArray versionData = i.toArray();
        Versions version;
        version.answer[0] = versionData.at(0).toString();
        version.answer[1] = versionData.at(1).toString();
        version.answer[2] = versionData.at(2).toString();
        version.answer[3] = versionData.at(3).toString();
        // Add data to list.
        m_versions.append(version);
    }
    endInsertRows();
    return true;
}

void VersionModel::setQuizName(const QString &quizName)
{
    m_versionNames = QStringList();
    m_versionNames.reserve(m_versions.size());
    // Combine the name.
    QString lowerName = quizName.toLower();
    lowerName.remove(' ');
    for(int i=0; i<m_versions.size(); ++i)
    {
        // Add name to version names.
        m_versionNames.append(lowerName + "v" + QString::number(i+1));
    }
}

VersionModel::Versions VersionModel::getVersion(int row) const
{
    if(row>-1 && row<m_versions.size())
    {
        return m_versions.at(row);
    }
    return Versions();
}

bool VersionModel::judge(const QString &title, const QList<QString> &answer,
                         int *matchedIndex, int *score)
{
    // Check the title similarity.
    int maxSim = -1, maxIndex = -1;
    for(int i=0; i<m_versionNames.size(); ++i)
    {
        int currentSim = similarity(title, m_versionNames.at(i));
        if(maxSim == -1 || currentSim < maxSim)
        {
            maxSim = currentSim;
            maxIndex = i;
        }
    }
    // Save the index.
    *matchedIndex = maxIndex+1;
    // Calculate score.
    maxSim = 0;
    const Versions matchedVersion = m_versions.at(maxIndex);
    for(int i=0; i<4; ++i)
    {
        maxSim += (answer.at(i) == matchedVersion.answer[i]);
    }
    *score = maxSim;
    return true;
}

int VersionModel::similarity(QString string1, QString string2)
{
    //Initial the size.
    const unsigned len1=string1.size(), len2=string2.size();
    std::vector<int> col(len2+1), prevCol(len2+1);
    //Fills the vector with ascending numbers, starting by 0
    //Because of the FUCK clang, we can NOT use itoa, or else these thing
    //following can be done in only one sentence:
    for(unsigned i=0; i<prevCol.size(); i++)
    {
        prevCol[i]=i;
    }
    //Use double std::min instead of std::min({,,}).
    for(unsigned i=0; i<len1; i++)
    {
        col[0]=i+1;
        for(unsigned j=0; j<len2; j++)
        {
            col[j+1]=std::min(std::min(1+col[j],
                                       1+prevCol[1+j]),
                    prevCol[j]+(string1[i]!=string2[j]));
        }
        std::swap(col, prevCol);
    }
    //Force cast the size_t to int
    return prevCol[len2];
}
