#ifndef SCOREDETAILVIEW_H
#define SCOREDETAILVIEW_H

#include <QDockWidget>

class QLabel;
class ClickableLabel;
class ScoreDetailView : public QDockWidget
{
    Q_OBJECT
public:
    explicit ScoreDetailView(QWidget *parent = nullptr);

signals:

public slots:
    void clearDetail();
    void setDetail(const QString &uniId, const QString &version,
                   const QString &filePath,
                   const QString &ans0, const QString &ansc0,
                   const QString &ans1, const QString &ansc1,
                   const QString &ans2, const QString &ansc2,
                   const QString &ans3, const QString &ansc3,
                   const QString &score);

private:
    inline QLabel *createLabel(Qt::Alignment align = Qt::AlignLeft | Qt::AlignVCenter);
    inline QLayout *createAnswerLayout(QLabel *ans, QLabel *cans);
    ClickableLabel *m_filePath;
    QLabel *m_uniId, *m_version,
           *m_answer0, *m_answer1, *m_answer2, *m_answer3,
           *m_answerC0, *m_answerC1, *m_answerC2, *m_answerC3,
           *m_score;
};

#endif // SCOREDETAILVIEW_H
