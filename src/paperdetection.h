#ifndef PAPERDETECTION_H
#define PAPERDETECTION_H

#include <QRgb>

#include <QObject>

class PaperDetection : public QObject
{
    Q_OBJECT
public:
    explicit PaperDetection(QObject *parent = nullptr);

    static void initCharSource();

    static QImage getCharSource(int index);

    static QImage binarize(const QImage &original);

    static QList<QPoint> findContours(const QImage &binImage);

    static bool correctHorizontal(QImage &binImage);

    static QString readTitle(const QImage &correctImage);

    static QString readUniId(const QImage &correctImage);

    static QList<QString> readAnswer(const QImage &correctImage);

    static QImage debugImg;

signals:

public slots:

private:
    static char detectChar(QImage itemImg);

    static int samePixels(const QImage &target, const QImage &source);

    static char readUniColumn(const QImage &correctColumn);

    static QString readAnswerItem(const QImage &answerItem);

    static bool isSelected(const QImage &img, int *count = nullptr);

    static QRect getBorder(const QList<QPoint> &borderPoints,
                           int entireWidth, int entireHeight);

    static QList<QImage> m_itemSource;
    static QRgb m_blackPixel;
};

#endif // PAPERDETECTION_H
