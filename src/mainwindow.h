#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

class Themis;
class ScoreDetailView;
class Test;
class TestView;
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void requireStartTest();

public slots:

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void onNewTest();
    void onOpenTest();
    bool onSaveTest();
    bool onSaveAsTest();
    bool onClose();

    void startTest();
    void stopTest();

    void onCompleteDetect(const QString &uniId,
                          const QString &title,
                          const QString &filePath,
                          const QList<QString> &answer);

private:
    inline bool processChange();

    inline QMenu *createMenu(const QString &caption);
    inline QAction *createAction(const QString &title,
                                 const QKeySequence &shortcut = QKeySequence());

    QThread m_testThread;
    TestView *m_testView;
    Test *m_test;
    Themis *m_testMachine;
    uint m_createCounter;

    ScoreDetailView *m_detailView;
};

#endif // MAINWINDOW_H
