#include <QPainter>

#include "scoredetailimageview.h"

ScoreDetailImageView::ScoreDetailImageView(QWidget *parent) : QDockWidget(parent)
{
}

void ScoreDetailImageView::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    if(m_image.isNull())
    {
        return;
    }
    QPainter painter(this);
    QPixmap scaledImage = m_image.scaled(size(),
                                         Qt::KeepAspectRatio,
                                         Qt::SmoothTransformation);
    painter.drawPixmap((width()-scaledImage.width())>>1,
                       (height()-scaledImage.height())>>1,
                       scaledImage);
}
