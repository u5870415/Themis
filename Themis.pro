QT += core gui widgets
CONFIG += c++11

SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/paperdetection.cpp \
    src/configuration.cpp \
    src/console.cpp \
    src/testview.cpp \
    src/test.cpp \
    src/finalscoreview.cpp \
    src/finalscoremodel.cpp \
    src/sheetmodel.cpp \
    src/versionmodel.cpp \
    src/themis.cpp \
    src/scoredetailview.cpp \
    src/scoredetailimageview.cpp \
    src/clickablelabel.cpp

HEADERS += \
    src/mainwindow.h \
    src/paperdetection.h \
    src/configuration.h \
    src/console.h \
    src/testview.h \
    src/test.h \
    src/finalscoreview.h \
    src/finalscoremodel.h \
    src/sheetmodel.h \
    src/versionmodel.h \
    src/themis.h \
    src/scoredetailview.h \
    src/scoredetailimageview.h \
    src/clickablelabel.h

RESOURCES += \
    src/res.qrc
